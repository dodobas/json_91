/*
 * Author: Andrew Dunstan
 * Created at: Sun Feb 05 09:43:31 -0500 2012
 *
 */ 

CREATE TYPE json;

CREATE FUNCTION json_in(cstring)
RETURNS json
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_out(json)
RETURNS cstring
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_recv(internal)
RETURNS json
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_send(json)
RETURNS bytea
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE TYPE json (
        INTERNALLENGTH = -1,
        INPUT = json_in,
        OUTPUT = json_out,
        RECEIVE = json_recv,
        SEND = json_send,
        STORAGE = extended
);

CREATE FUNCTION array_to_json(anyarray)
RETURNS json
AS 'MODULE_PATHNAME','array_to_json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION array_to_json(anyarray, bool)
RETURNS json
AS 'MODULE_PATHNAME','array_to_json_pretty'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION row_to_json(record)
RETURNS json
AS 'MODULE_PATHNAME','row_to_json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION row_to_json(record, bool)
RETURNS json
AS 'MODULE_PATHNAME','row_to_json_pretty'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION query_to_json(text)
RETURNS json
AS 'MODULE_PATHNAME','query_to_json'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION query_to_json(text, bool)
RETURNS json
AS 'MODULE_PATHNAME','query_to_json_pretty'
LANGUAGE C STRICT IMMUTABLE;